group = "org.example.my-app"

plugins {
    id("java")
    kotlin("jvm")
    id("checkstyle")
}

val myBuildGroup = "my project build"
tasks.named<TaskReportTask>("tasks") {
    displayGroup = myBuildGroup
}

tasks.build {
    group = myBuildGroup
}

tasks.check {
    group = myBuildGroup
    description = "Runs checks (including tests)."
}

tasks.register("qualityCheck") {
    group = myBuildGroup
    description = "Runs checks (excluding tests)."
    dependsOn(tasks.classes, tasks.checkstyleMain)
    dependsOn(tasks.testClasses, tasks.checkstyleTest)
}

java {
    toolchain.languageVersion.set(JavaLanguageVersion.of(17))
}

tasks.test {
    useJUnitPlatform()
    testLogging.showStandardStreams = true
}

kotlin {
    jvmToolchain(17)
}

dependencies {
    constraints {
        implementation("org.apache.commons:commons-lang3:3.9")
        implementation("com.google.errorprone:error_prone_annotations:2.9.0")
        implementation("org.slf4j:slf4j-api:1.7.32")
        implementation("org.slf4j:slf4j-simple:1.7.32")
    }
    testImplementation("org.junit.jupiter:junit-jupiter:5.10.0")
}
