rootProject.name = "my-build-logic"

dependencyResolutionManagement {
    repositories.gradlePluginPortal()
}

include("java-plugins")