rootProject.name = "my-project"

pluginManagement {
    repositories.gradlePluginPortal()
    includeBuild("../my-build-logic")
    includeBuild(".")
}

dependencyResolutionManagement {
    repositories.mavenCentral()
    includeBuild("../my-other-project")
}

rootDir.listFiles()
    .filter { it.isDirectory }
    .filterNot { it.isHidden }
    .forEach { include(it.name) }

